FROM node:8-alpine
WORKDIR /app
ADD package.json /app
RUN npm install
COPY . /app
CMD ["npm", "start"]